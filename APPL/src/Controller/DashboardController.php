<?php
// src/Controller/DashboardController.php
namespace App\Controller;

use App\Entity\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController {
    /**
    * @Route("/")
    */
    public function initializeDashboard() {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return new Response(
            '<html><body>Lucky number:</body></html>'
        );
    }

//    public function createOrganization() {
//        $em = $this->getDoctrine()->getManager();
//
//        $organization = new Organization();
//        $organization->setTestTest1("test".rand(0, 999));
//        $em->persist($organization);
//        $em->flush();
//
//        return new Response(
//            '<html><body>Lucky number: '.$organization->getId().'</body></html>'
//        );
//    }
}