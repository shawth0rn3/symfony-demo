<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class ProductImage extends Base {

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $image;



}