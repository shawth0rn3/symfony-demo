<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class Organization extends Base {

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $testTest1;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="organization")
     * @var Product[]
     */
    private $products;

    /**
     * @return string
     */
    public function getTestTest1(): string {
        return $this->testTest1;
    }

    /**
     * @param string $testTest1
     */
    public function setTestTest1(string $testTest1): void {
        $this->testTest1 = $testTest1;
    }



}