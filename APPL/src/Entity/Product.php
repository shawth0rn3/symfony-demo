<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class Product extends Base {

    /**
     * @ORM\ManyToOne(targetEntity="Organization")
     */
    private $organization;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product")
     * @var ProductImage[]
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="ProductType")
     * @var ProductType[]
     */
    private $types;


}