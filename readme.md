**symfony backend demo**

*steps followed:*

1. docker exec -ti symfony-demo-web /bin/bash
2. composer create-project symfony/skeleton app
3. open http://localhost:8080/
4. composer require symfony/orm-pack
5. composer require --dev symfony/maker-bundle
6. edited database url in APPL/.env
7. Fixed some database connection problems for doctrine


**resources**

https://symfony.com/doc/current/setup.html    
https://symfony.com/doc/current/doctrine.html    


**useful commands**    

 php bin/console debug:router    
 php bin/console doctrine:schema:update --force   
 